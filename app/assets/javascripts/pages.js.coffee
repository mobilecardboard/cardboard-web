# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  palette = $('#palette')

  moveMag = '40'
  zoomSpeed = 100

  window.panUp = ->
    palette.animate 'top': '-=' + moveMag, 200

  window.panDown = ->
    palette.animate 'top': '+=' + moveMag, 200

  window.panLeft = ->
    palette.animate 'left': '-=' + moveMag, 200

  window.panRight = ->
    palette.animate 'left': '+=' + moveMag, 200

  window.zoomIn = ->
    palette.animate 'height': '+=' + moveMag, zoomSpeed
    palette.animate 'width': '+=' + moveMag, zoomSpeed


  window.zoomOut = ->
    palette.animate 'height': '-=' + moveMag, zoomSpeed
    palette.animate 'width': '-=' + moveMag, zoomSpeed



  $('#pan_up').click ->
    panUp()

  $('#pan_down').click ->
    panDown()

  $('#pan_left').click ->
    panLeft()

  $('#pan_right').click ->
    panRight()

  $('#zoom_in').click ->
    zoomIn()

  $('#zoom_out').click ->
    zoomOut()

  # deviceOrientationHandler = (alpha, beta, gamma) ->
  #   console.log "firing handler"

  #   $('#alpha').val alpha
  #   $('#beta').val beta
  #   $('#gamma').val gamma


  # deviceMotionHandler = (evtData) ->

  #   acceleration = evtData.acceleration

  #   x = acceleration.x
  #   y = acceleration.y
  #   z = acceleration.z

  #   magnitude = Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)

  #   $('#motion_x').val x
  #   $('#motion_y').val y
  #   $('#motion_z').val z


  # if window.DeviceOrientationEvent
  #   alert "DeviceOrientation suppored!"


  #   window.addEventListener 'deviceorientation', (evtData) ->
  #     console.log "firing listener"

  #     lr = evtData.gamma
  #     fb = evtData.beta
  #     dir = evtData.alpha

  #     deviceOrientationHandler dir, fb, lr

  #   , false


  # if window.DeviceMotionEvent
  #   alert "DeviceMotionEvent supported!"

  #   window.addEventListener 'devicemotion', deviceMotionHandler, false